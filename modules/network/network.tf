resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "PRA-VPC-MAIN"
  }
}


#PUBLIC and PRIVATE SUBNET 

resource "aws_subnet" "PRA-PUBsubnet" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = var.demo-subnet-public-1_cidr
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "PRA-PUBsubnet"
  }
}
resource "aws_subnet" "PRA-PUBsubnet2" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = var.demo-subnet-public-2_cidr
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "PRA-PUBsubnet2"
  }
}

# Creating Private Subnet for database
resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.demo-subnet-private-1_cidr

  tags = {
    Name = "Terraform-PVT"
  }
}

#IGW

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "PRA-IGW-TERRAFORM"
  }
}
# Allowing default route table to go to Internet Gateway 
resource "aws_default_route_table" "gw_router" {
  default_route_table_id = aws_vpc.my_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "default table"
  }
}

#EC2 Instance creation
resource "aws_instance" "web" {
  ami           = var.ami
  instance_type = var.instance_type
  subnet_id     = aws_subnet.PRA-PUBsubnet.id
  tags = {
    Name = "PRA-EC2-Test"
  }
}
#### AMI Creation for Nginx Instance#####
resource "aws_ami_from_instance" "example" {
  name               = "terraform-example"
  source_instance_id = aws_instance.web.id
}



##### ALB Security Group ######
resource "aws_security_group" "lb-sg" {
  name        = "Demo-lb-sg"
  description = "Load balancer security group"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["27.59.230.155/32"]
  }



  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["165.225.122.108/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }



  tags = {
    Name = "LB-Security-Group"
  }
}
######## ALB ############
resource "aws_lb" "demo-lb" {
  name               = "demo-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.lb-sg.id}"]
  subnets            = ["${aws_subnet.PRA-PUBsubnet.id}", "${aws_subnet.PRA-PUBsubnet2.id}"]
  tags = {
    Name = "Demo-LB"
  }
}
##### ALB Target Group
resource "aws_alb_target_group" "lb-tg" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }
  name     = "demo-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.my_vpc.id
}
###### LB Listner #####
resource "aws_alb_listener" "lb-listner" {
  load_balancer_arn = aws_lb.demo-lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_alb_target_group.lb-tg.arn
    type             = "forward"
  }
}
##### EC2 Security Group  ######
resource "aws_security_group" "ec2-sg" {
  name        = "Demo-ec2-sg"
  description = "EC2 security group"
  vpc_id      = aws_vpc.my_vpc.id



  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["27.59.230.155/32"]
  }



  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["165.225.122.108/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }



  tags = {
    Name = "EC2-Security-Group"
  }
}


resource "aws_launch_configuration" "web" {
  name_prefix   = "web-"
  image_id      = aws_ami_from_instance.example.id # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = "t2.micro"
  key_name      = "PRA"


  security_groups             = [aws_security_group.ec2-sg.id]
  associate_public_ip_address = true




  lifecycle {
    create_before_destroy = true
  }






}
resource "aws_autoscaling_group" "web" {
  name = "${aws_launch_configuration.web.name}-asg"


  min_size         = 1
  desired_capacity = 2
  max_size         = 4



  launch_configuration = aws_launch_configuration.web.name


  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]


  metrics_granularity = "1Minute"


  vpc_zone_identifier = [
    aws_subnet.PRA-PUBsubnet.id,
    aws_subnet.PRA-PUBsubnet2.id
  ]


  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {

    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }
}














